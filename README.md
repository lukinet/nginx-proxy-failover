# High-Availability Nginx Proxy Setup

## Prerequisits
- Vagrant
- Virtualbox
- Ansible
- IP Range 10.0.0.0/24 not currently in use in local network

## Usage
### 1. Start VMs
```
vagrant up
```

### 2. Test Connection
```
ansible all -a "/bin/echo hello"
```

### 3. Provision VMs
```
ansible-playbook site.yml
```
